<?php

use App\Models\{
    User,
    Preference,
    Course,
    Permission,
    Image,
    Comment,
    Tag
};
use Illuminate\Support\Facades\Route;

Route::get('/one-to-one', function () {
    $user = User::with('preference')->find(2);

    $data = [
        'background_color' => '#000',
    ];

    if ($user->preference) {
        $user->preference->update($data);
    } else {
        $preference = new Preference($data);
        $user->preference()->save($preference);
    }

    $user->refresh();

    // dd($user);
    var_dump($user->preference);

    $user->preference->delete();
    $user->refresh();

    dd($user->preference);
});

Route::get('/one-to-many', function () {
    // $course = Course::create(['name' => 'Curso de Laravel']);
    $course = Course::with('modules.lessons')->first();


    // $dataModulo1 = [
    //     'name' => 'Módulo x1'
    // ];
    // $course->modules()->create($dataModulo1);

    // $dataModulo2 = [
    //     'name' => 'Módulo x2'
    // ];
    // $course->modules()->create($dataModulo2);

    // $course->modules()->get(); - Faz uma consulta no banco a mais
    $modules = $course->modules;


    // $dataAula1 = [
    //     'name' => 'Aula 1',
    //     'video' => 'link video'
    // ];
    // $modules->first()->lessons()->create($dataAula1);

    echo $course->name;
    echo '<br>';
    echo '<br>';
    foreach ($course->modules as $module) {
        echo "Módulo {$module->name} <br>";

        foreach ($module->lessons as $lesson) {
            echo "Aula {$lesson->name} <br>";
        }
        echo '<br>';
    }

    // dd($modules);
});

Route::get('/many-to-many', function () {
    // Permission::create(['name' => 'menu1']);
    // Permission::create(['name' => 'menu2']);
    // Permission::create(['name' => 'menu3']);

    // $permission = Permission::all();
    // dd($permission);

    $user = User::with('permissions')->find(1);

    // $permission = Permission::find(1);
    // $user->permissions()->save($permission);
    // $user->permissions()->saveMany([
    //     Permission::find(1),
    //     Permission::find(3),
    //     Permission::find(2),
    // ]);
    // $user->permissions()->sync([2]);
    // $user->permissions()->attach([1, 3]);
    // $user->permissions()->detach([1, 3]);

    $user->refresh();

    dd($user->permissions);
});

Route::get('/many-to-many-pivot', function () {
    $user = User::with('permissions')->find(1);
    // $user->permissions()->attach([
    //     1 => ['active' => false],
    //     3 => ['active' => false],
    // ]);

    echo "<b>{$user->name}</b><br>";
    foreach ($user->permissions as $permssion) {
        dd($permssion->pivot);
        echo "{$permssion->name} - {$permssion->pivot->active} <br>";
    }
});

Route::get('/one-to-one-polymorphic', function () {
    $user = User::first();

    $data = ['path' => 'path/nome-image.png'];

    // $user->image->delete();

    if ($user->image) {
        $user->image->update($data);
    } else {
        // $user->image()->save(new Image($data));
        $user->image()->create($data);
    }

    dd($user->image->path);
});

Route::get('/one-to-many-polymorphic', function () {
    // $course = Course::first();

    // $course->comments()->create([
    //     'subject' => 'Novo Comentário 2',
    //     'content' => 'Apenas (2) um comentário legal',
    // ]);

    // dd($course->comments);

    $comment = Comment::find(1);
    dd($comment->commentable);
});

Route::get('/many-to-many-polymorphic', function () {
    // Tag::create(['name' => 'tag1', 'color' => 'blue']);
    // Tag::create(['name' => 'tag2', 'color' => 'red']);
    // Tag::create(['name' => 'tag3', 'color' => 'green']);

    // $course = Course::first();
    // $course->tags()->attach(2);
    // dd($course->tags);

    $tag = Tag::where('name', 'tag2')->first();
    dd($tag->courses);
});
